import java.util.concurrent.atomic.AtomicInteger;

/**
 * Décrivez votre classe Vehicule ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 * 
 */
public abstract class Vehicule
{

    // la cate ́gorie du ve ́hicule (« ve ́lo », « VTT », « remorque »)
    private final String categorie;
    // le nombre d’heures total de location pour ce ve ́hicule
    private int nbHeuresLocation = 0;
    // le nombre d’heures de location depuis le dernier en- tretien
    private int nbHeuresDepuisEntretien = 0;
    // indique si le ve ́hicule est disponible a` la location 
    private boolean disponible= true;
    // identifiant du véhicule
    private final int numeroVehicule;
    // compteur d'identification des véhicules
    private static final AtomicInteger compteurNumeroVehicule = new AtomicInteger();

    /**
     * Constructeur d'objets de classe Vehicule
     */
    protected Vehicule(String categorie)
    {
        // Numérote le véhicule et incrément le compteur d'identification
        numeroVehicule = compteurNumeroVehicule.incrementAndGet();
        // Initialsation des attributs
        this.categorie = categorie;
        this.nbHeuresLocation = 0;
        this.nbHeuresDepuisEntretien = 0;
        this.disponible = true;

    }

    /**
     * Ajouter un nombre d’heures de location a` nbHeuresLocation et nbHeuresDepuisEntretien
     */
    public void ajoutHeuresLocation(int heures)
    {
        nbHeuresLocation += heures;
        nbHeuresDepuisEntretien += heures;
    }

    /**
     * Faire entretien
     */
    public void faireEntretien()
    {
        nbHeuresDepuisEntretien = 0;
        rendreDisponible();
    }

    /**
     * Obtenir nombre d'heures de location
     */
    public int getNbHeuresLocation()
    {
        return nbHeuresLocation;
    }

    /**
     * Obtenir nombre d'heures depuis l'entretien
     */
    public int getNbHeuresDepuisEntretien()
    {
        return nbHeuresDepuisEntretien;
    }

    /**
     * Obtenir disponibilité pour al location
     */
    public boolean estDisponible()
    {
        return disponible;
    }

    /**
     * Rendre disponible un véhicule
     */
    public void rendreDisponible()
    {
        disponible = true;
    }

    /**
     * Rendre indisponible un véhicule
     */
    public void rendreIndisponible()
    {
        disponible = false;
    }

    /**
     * Méthode entretienNecessaire
     *
     * @return vrai si l'entretien est nécessaire
     */
    public abstract boolean entretienNecessaire();

    /**
     * Méthode toString
     *
     * @return les informations sur le véhicule
     */
    public String toString()
    {
        String informations = "";
        informations += "Véhicule n° " + numeroVehicule;
        informations += ", de catégorie : " + categorie;
        informations += " : heures de location: " + nbHeuresLocation;
        informations += ", heures depuis l'entretien: " + nbHeuresDepuisEntretien;
        return informations;
    }
}
