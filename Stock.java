
/**
 * Décrivez votre classe Stock ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Stock
{

    public static final int MAX_VEHICULES = 100;
    private int nbVehicules;
    private Vehicule [] vehicules ;

    public Stock() {
        vehicules = new Vehicule [MAX_VEHICULES];
        nbVehicules = 0;
    }

    public int getNbVehicules() { return nbVehicules; }

    public  void ajoute(Vehicule v) {
        vehicules[nbVehicules] = v;
        nbVehicules++;
    }

    public String toString() {
        StringBuffer stock = new StringBuffer ();;
        for (int i=0; i < nbVehicules; i++) {
            stock.append(vehicules[i].toString());
            stock.append("\n");
        }
        return stock.toString();
    }

    public String listerEntretiens() {
        StringBuffer liste = new StringBuffer ();
        for (int i=0; i < nbVehicules; i++) {
            if (vehicules[i].entretienNecessaire()) {
                liste.append(vehicules[i].toString());
                liste.append("\n");
            }
        }
        return liste.toString();
    }

    public boolean locationVeloRemorquePosibble(int nbPlaces) {
        boolean veloAvecAttache = false;
        boolean remorque = false;
        for (int i=0; i < nbVehicules; i++) {
            Vehicule v = vehicules[i];
            if (v.estDisponible()) {
                if (v instanceof Remorque) {
                    remorque = remorque || (nbPlaces == ((Remorque)v).getNbPlaces());
                }
                if (v instanceof Velo) {
                    veloAvecAttache = veloAvecAttache || ((Velo)v).getAttacheRemorquePresente();
                } 
            }
        }
        return veloAvecAttache && remorque;
    }

    public static void main(String[] args) {
        Stock s = new Stock();
        Velo v1 = new Velo();
        v1. setAttacheRemorquePresente (true );
        s.ajoute(v1);
        Velo v2 = new VTT();
        Velo v3 = new VTT();
        s.ajoute(v2);
        s.ajoute(v3);
        Remorque r1 = new Remorque(1);
        Remorque r2 = new Remorque(2);
        s.ajoute(r1);
        s.ajoute(r2);

        System.out.println(s);
        System.out.println(s.listerEntretiens());

        if (s.locationVeloRemorquePosibble(2)) {
            System.out.println("Il est possible de louer un vélo avec attache et uen remorque 2 places.");
        }

    }
}
