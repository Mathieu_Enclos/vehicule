
/**
 * Décrivez votre classe Remorque ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Remorque extends Vehicule implements Louable
{

    // limite d'usage avant l'entretien
    private static final int USAGELIMITE = 20;
    // Nombre limite de places dans uen remorque
    private static final int LIMITE_PLACES = 2;
    // nbPlaces indique le nombre de places (1 ou 2) de la remorque
    private final int nbPlaces;

    /**
     * Constructeur d'objets de classe Remorque
     */
    public Remorque(int nbPlaces)
    {
        super("remorque");
        if ((nbPlaces < 1) && (nbPlaces > LIMITE_PLACES)) {
            throw new IllegalArgumentException("le nombre de places d'une remorque est 1 ou 2 et pas "+nbPlaces);
        }
        this.nbPlaces = nbPlaces;
    }

    public int getNbPlaces() 
    {
        return nbPlaces;
    }

    public String toString()
    {
        return super.toString() + ", nombre de places : " + getNbPlaces();
    }

    /**
     * Méthode entretienNecessaire
     *
     * @return un booléen qui indique si l'entretien est nécessaire
     */
    public boolean entretienNecessaire()
    {
        return (getNbHeuresDepuisEntretien() > USAGELIMITE);
    }

    /**
     * Permet de louer un véhicule
     *
     * @return booleéen qui indique si la location a puˆ eˆtre possible.
     */
    public boolean louer()
    {
        if (estDisponible()) {
            rendreIndisponible();
            return true;
        }
        return false;
    }

    /**
     * Permet de rendre le vé́hicule à la fin de la durée de la location
     *
     * @param duree nombre d'heures de location
     */
    public void rendre(int duree)
    {
        ajoutHeuresLocation(duree);
        if (! entretienNecessaire()) {
            rendreDisponible();
        }
    }
}
