
/**
 * Décrivez votre classe Velo ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Velo extends Vehicule implements Louable
{
    // limite d'usage avant l'entretien
    private static final int USAGELIMITE = 30;
    // indique si une attache re- morque a e ́te ́ installe ́e sur le ve ́lo
    private boolean attacheRemorquePresente = false;

    /**
     * Constructeur d'objets de classe Velo
     */
    public Velo()
    {
        super("Vélo");
    }

    /**
     * Constructeur d'objets de classe Velo
     */
    public Velo(String categorie)
    {
        super(categorie);
    }

    public boolean getAttacheRemorquePresente()
    {
        return attacheRemorquePresente;
    }

    public void setAttacheRemorquePresente(boolean presence)
    {
        attacheRemorquePresente = presence;
    }

    /**
     * Méthode entretienNecessaire
     *
     * @return un booléen qui indique si l'entretien est nécessaire
     */
    public boolean entretienNecessaire()
    {
        return (getNbHeuresDepuisEntretien() > USAGELIMITE);
    }

    /**
     * Permet de louer un véhicule
     *
     * @return booleéen qui indique si la location a puˆ eˆtre possible.
     */
    public boolean louer()
    {
        if (estDisponible()) {
            rendreIndisponible();
            return true;
        }
        return false;
    }

    /**
     * Permet de rendre le vé́hicule à la fin de la durée de la location
     *
     * @param duree nombre d'heures de location
     */
    public void rendre(int duree)
    {
        ajoutHeuresLocation(duree);
        if (! entretienNecessaire()) {
            rendreDisponible();
        }
    }
}
