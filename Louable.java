
/**
 * Décrivez votre interface Louable ici.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 */

public interface Louable
{

    /**
     * Permet de louer un véhicule
     *
     * @return booleéen qui indique si la location a puˆ eˆtre possible.
     */
    boolean louer();

    /**
     * Permet de rendre le vé́hicule à la fin de la durée de la location
     *
     * @param duree nombre d'heures de location
     */
    void rendre(int duree);
}
